import xmlrpc.client
import socket
#client tidak langsung terhubung ke server tetapi melalui stub (proxy)

s = xmlrpc.client.ServerProxy('http://localhost:8018')

def tambah():
	angka = int(input("Ingin operasi berapa angka? "))
	i = 0 
	arr = []
	for x in range (angka):
		bil = int(input("-> masukkan angka: ".format(i)))
		arr.append(bil)
	print(s.add(arr))
	ask()
def kurang():
	angka = int(input("Ingin operasi berapa angka? "))
	i = 0 
	arr = []
	for x in range (angka):
		bil = int(input("-> masukkan angka: ".format(i)))
		arr.append(bil)
	print(s.min(arr))
	ask()
def kali():
	angka = int(input("Ingin operasi berapa angka? "))
	i = 0 
	arr = []
	for x in range (angka):
		bil = int(input("-> masukkan angka: ".format(i)))
		arr.append(bil)
	print(s.mul(arr))
	ask()
def bagi():
	angka = int(input("Ingin operasi berapa angka? "))
	i = 0 
	arr = []
	for x in range (angka):
		bil = int(input("-> masukkan angka: ".format(i)))
		arr.append(bil)
	print(s.div(arr))
	ask()
def pangkat():
	angka = int(input("-> Masukkan angka: "))
	angka2 = int(input("-> Masukkan pangkat: "))
	print(s.pow(angka, angka2))
	ask()
def fung_akar():
	angka = int(input("-> Masukkan akar: "))
	print(s.akar(angka))
	ask()
def fung_sin():
	angka = int(input("-> Masukkan sin: "))
	print(s.sin(angka))
	ask()
def fung_cos():
	angka = int(input("-> Masukkan cos: "))
	print(s.cos(angka))
	ask()
def ask():
	print("==============================")
	tanya = input("Apakah anda ingin mengulang (Y/T)?")
	if tanya =='Y' or tanya =='y':
		menu()
	elif tanya =='T' or tanya =='t':
		print("==============================")
		print("Terima kasih sudah menggunakan program ini")
		print("==============================")
	else :
		print("==============================")
		print("maaf, inputan salah")
		print("silahkan masukkan Y atau T")
		ask()
def menu():
	print("=========PILIH OPERASI========")
	print("1. Penjumlahan	")
	print("2. Pengurangan	")
	print("3. Perkalian		")
	print("4. Pembagian		")
	print("5. Perpangkatan 	")
	print("6. Akar Kuadrat 	")
	print("7. Trigonometri Sin	")
	print("8. Trigonometri Cos	")
	print("==============================")
	pilihan = input("Masukkan pilihan operasi(1/2/3/4/5/6/7/8): ")

	if pilihan == '1':
		tambah()
	elif pilihan =='2':
		kurang()
	elif pilihan =='3':
		kali()
	elif pilihan =='4':
		bagi()
	elif pilihan =='5':
		pangkat()
	elif pilihan =='6':
		fung_akar()
	elif pilihan =='7':
		fung_sin()
	elif pilihan =='8':
		fung_cos()
	else:
		print("===============================")
		print("Inputan yang anda masukkan salah")
		ask()
menu()

