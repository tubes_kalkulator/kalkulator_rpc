Server
------------

Hal yang pertama dilakukan yaitu IMPORT library yang akan digunakann

```
from xmlrpc.server import SimpleXMLRPCServer
from xmlrpc.server import SimpleXMLRPCRequestHandler
```

* Kemudian buat class Requesthandler dengan membatasi path RPC yaitu /RPC2,  sehingga sistem hanya mengakses path RPC2 saja dan tidak perlu mengakses semua path yang disediakan oleh RPC

```
class RequestHandler(SimpleXMLRPCRequestHandler):
	rpc_path = ('/RPC2',)
```

* Membuat server dengan mendefinisikan ip dan port yang akan digunakan
```
with SimpleXMLRPCServer(("localhost", 8018), requestHandler=RequestHandler) as server:
	server.register_introspection_functions()

```
* Masih didalam server, buat fungsi untuk penjumlahan. Nah disini banyak nya angka yang akan dioperasikan disimpan didalam array. Sehingga kita membuat perulanngan didalam array.
dengan membuat variabel baru untuk menyimpan hasil penjumlahan setiap kali melakukan iterasi. Kita definisikan dengan 0. Lalu setiap melakukan perulangan sebanyak jumlah elemen yang ada diarray, hasil tersebut akan diupdate dengan `hasil=hasil+i`
```
    def adder_function(x):
	    	hasil = 0
		    for i in x:
			    hasil += i
		    return hasil
	server.register_function(adder_function, 'add')
```
* sama seperti fungsi penjumlahan, fungsi pengurangan juga dilakukan perulangan sebanyak jumlah elemen yang tersimpan dalam array. Sistemnya ialah melakukan pengurangan untuk setiap angka setelah elemen array pertama.
```
    def substract_function (x):
		if len(x)>0:
			return x[0] - sum(x[1:])
		else: 
			return none
	server.register_function(substract_function, 'min')
```
* Selanjutnya yaitu membuat fungsi bagi. Struktur fungsinya hampir sama dengan pengurangan. Perbedaannya hanya terletak pada operasi yang digunakan yaitu bagi. 
```
    def divide_function (x):
		if len(x)>0:
			return x[0] / sum(x[1:])
		else: 
			return none
	server.register_function(divide_function, 'div')
```
* Untuk fungsi perkalian sendiri, pendefinisian variabel untuk menyimpan hasilnya yaitu 1. Karena apapun yang dikalikan dengan satu yaitu angka itu sendiri.
```
    class Myfunction:
		def mul(self,x):
			hasil = 1
			for i in x:
				hasil *=i
			return hasil
	server.register_instance(Myfunction())
```
* Membuat fungsi/prosedur Pangkat
```
 	server.register_function(pow)
```

* Membuat fungsi/prosedur Akar kuadrat
```
 	def akar_function (x):
		hasil = math.sqrt(x)
		return hasil
	server.register_function(akar_function, 'akar')
```
* Membuat fungsi/prosedur Trigonometri sin
```
 	def sin_function (x):
		hasil = math.sin(x)
		return hasil
	server.register_function(sin_function, 'sin')
```
* Membuat fungsi/prosedur Trigonometri cos
```
 	def cos_function (x):
		hasil = math.cos(x)
		return hasil
	server.register_function(cos_function, 'cos')
```
* Selanjutnya print tampilan bahawa server siap digunakan untuk memberi pesan kepada pengguna bahwa server telah siap
```
 	print ("Server siap digunakan...")
	server.serve_forever()
```
### Untuk server telah selesai , selanjutnya membuat CLIENT.

Client
-----------
* Import library untuk client
```
import xmlrpc.client
import socket
```
* mendefinisikan ip dan port
```
s = xmlrpc.client.ServerProxy('http://localhost:8018')
```
* Membagi setiap metod yang ada, dan setiap metod berisi inputan user yang akan di simpan kedalam array yang kemudian akan dieksekusi pada server.
pada setiap metod yang ada juga dilakukan pemanggilan terhadap prosedur yang telah dibuat diserver.
Banyaknya inputan user akan disimpan kedalam array, lalu dilakukan perulangan sebanyak angka tersebut untuk  meminta user memasukkan angka yang ingin dioperasikan.
setelah itu baru dilakukan pemanggilan prosedur.
```
def tambah():
	angka = int(input("Ingin operasi berapa angka? "))
	i = 0 
	arr = []
	for x in range (angka):
		bil = int(input("-> masukkan angka: ".format(i)))
		arr.append(bil)
	print(s.add(arr))
	ask()
def kurang():
	angka = int(input("Ingin operasi berapa angka? "))
	i = 0 
	arr = []
	for x in range (angka):
		bil = int(input("-> masukkan angka: ".format(i)))
		arr.append(bil)
	print(s.min(arr))
	ask()
def kali():
	angka = int(input("Ingin operasi berapa angka? "))
	i = 0 
	arr = []
	for x in range (angka):
		bil = int(input("-> masukkan angka: ".format(i)))
		arr.append(bil)
	print(s.mul(arr))
	ask()
def bagi():
	angka = int(input("Ingin operasi berapa angka? "))
	i = 0 
	arr = []
	for x in range (angka):
		bil = int(input("-> masukkan angka: ".format(i)))
		arr.append(bil)
	print(s.div(arr))
	ask()
def pangkat():
	angka = int(input("-> Masukkan angka: "))
	angka2 = int(input("-> Masukkan pangkat: "))
	print(s.pow(angka, angka2))
	ask()
def fung_akar():
	angka = int(input("-> Masukkan akar: "))
	print(s.akar(angka))
	ask()
def fung_sin():
	angka = int(input("-> Masukkan sin: "))
	print(s.sin(angka))
	ask()
def fung_cos():
	angka = int(input("-> Masukkan cos: "))
	print(s.cos(angka))
	ask()
```
* Membuat metod ask, yang berisi pertanyaan apakah user masih tetap ingin melanjutkan menggunakan program atau tidak
```
def ask():
	print("==============================")
	tanya = input("Apakah anda ingin mengulang (Y/T)?")
	if tanya =='Y' or tanya =='y':
		menu()
	elif tanya =='T' or tanya =='t':
		print("==============================")
		print("Terima kasih sudah menggunakan program ini")
		print("==============================")
	else :
		print("==============================")
		print("maaf, inputan salah")
		print("silahkan masukkan Y atau T")
		ask()
```
* Membuat menu
```

def menu():
	print("=========PILIH OPERASI========")
	print("1. Penjumlahan	")
	print("2. Pengurangan	")
	print("3. Perkalian		")
	print("4. Pembagian		")
	print("5. Perpangkatan 	")
	print("6. Akar Kuadrat 	")
	print("7. Trigonometri Sin	")
	print("8. Trigonometri Cos	")	
	print("==============================")
	pilihan = input("Masukkan pilihan operasi(1/2/3/4/5/6/7/8): ")

	if pilihan == '1':
		tambah()
	elif pilihan =='2':
		kurang()
	elif pilihan =='3':
		kali()
	elif pilihan =='4':
		bagi()
	elif pilihan =='5':
		pangkat()
	elif pilihan =='6':
		fung_akar()
	elif pilihan =='7':
		fung_sin()
	elif pilihan =='8':
		fung_cos()
	else:
		print("===============================")
		print("Inputan yang anda masukkan salah")
		ask()
menu()

```