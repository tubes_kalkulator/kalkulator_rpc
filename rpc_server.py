from xmlrpc.server import SimpleXMLRPCServer
from xmlrpc.server import SimpleXMLRPCRequestHandler
import math

class RequestHandler(SimpleXMLRPCRequestHandler):
	rpc_path = ('/RPC2',)

with SimpleXMLRPCServer(("localhost", 8018), requestHandler=RequestHandler) as server:
	server.register_introspection_functions()

	def adder_function(x):
		hasil = 0
		for i in x:
			hasil += i
		return hasil
	server.register_function(adder_function, 'add')
	
	def substract_function (x):
		if len(x)>0:
			return x[0] - sum(x[1:])
		else: 
			return none
	server.register_function(substract_function, 'min')
	
	def divide_function (x):
		if len(x)>0:
			return x[0] / sum(x[1:])
		else: 
			return none
	server.register_function(divide_function, 'div')
	
	class Myfunction:
		def mul(self,x):
			hasil = 1
			for i in x:
				hasil *=i
			return hasil
	server.register_instance(Myfunction())
	
	#fungsi perpangkatan
	server.register_function(pow)
	
	#Fungsi akar kuadrat
	def akar_function (x):
		hasil = math.sqrt(x)
		return hasil
	server.register_function(akar_function, 'akar')
	
	#Fungsi nilai Sin
	def sin_function (x):
		hasil = math.sin(x)
		return hasil
	server.register_function(sin_function, 'sin')
	
	#Fungsi nilai Cos
	def cos_function (x):
		hasil = math.cos(x)
		return hasil
	server.register_function(cos_function, 'cos')
	print ("Server siap digunakan...")
	server.serve_forever()
