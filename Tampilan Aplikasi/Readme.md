Gambaran umum 
------------
Berikut gambaran umum tentang bagaimana sistem berjalan.

![flowchart](/uploads/a5aa562f90867e0b5d09898b860f69fa/flowchart.jpg)

Tampilan Menu 
-------------
- Ketika memilih menu 1 yaitu "penjumlahan"

![penjumlahan](/uploads/75fa921e48345dbe63f242a08b159d26/penjumlahan.PNG)

- Ketika memilih menu 2 yaitu "pengurangan"

![pengurangan](/uploads/2c08883e62f3367bdfbebd7054d3156b/pengurangan.PNG)

- Ketika memilih menu 3 yaitu "perkalian"

![perkalian](/uploads/c936bd1813aea5371c8587f66237abe3/perkalian.PNG)

- Ketika memilih menu 4 yaitu "pembagian"

![pembagian](/uploads/08572f12e104b4f6cd68869f4fc4cc33/pembagian.PNG)

- Ketika user salah menginputkan pilihan menu, maka sistem akan menampilkan pesan untuk inputan apa yang bisa diterima

![salah_menu](/uploads/e3d10f82a8ca1f1135924944f128008f/salah_menu.PNG)


- Ketika user salah memasukkan inputan ingin melanjutkan menggunakan sistem atau tidak

![salah_yt](/uploads/1b5f76a7d276599d536227406c21a7ad/salah_yt.PNG)


Update Tampilan menu  
----------------------
![up_menu](/uploads/53fc2baf41819a0d92069d46c8c66d9b/up_menu.PNG)


- Tampilan ketika memilih menu Perpangkatan

![up_pangkat](/uploads/45cba5a66748dde118c70f9bfe3f3662/up_pangkat.PNG)

- Tampilan ketika memilih menu Akar Kuadrat

![up_akar](/uploads/91ca61324c343be19da8158fb95c3bd4/up_akar.PNG)

- Tampilan ketika memilih menu Trigonometri Sin

![up__sin](/uploads/b62d7b46d0b6cc4c4706c3fa2e6ed958/up__sin.PNG)

- Tampilan ketika memilih menu Trigonometri Cos

![up_cos](/uploads/9c939a9dcbc0d21fd2c3cedfee3bd7ab/up_cos.PNG)