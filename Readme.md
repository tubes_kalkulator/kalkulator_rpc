NAMA PROJECT
-------
Kalkulator Sederhana menggunakan modul RPC 

TENTANG PROJECT
----------------
kalkulator sederhana ini merupakan kalkulator yang dibangun dengan bahasa JAVA. Menggunakan modul RPC dengan sistem `server`, `çlient`. 
RPC atau yang merupakan singkatan dari Remote procedure call memungkinkan kita untuk mengakses sebuah prosedur yang berada pada komputer lain. 
Untuk dapat melakukan ini sebuah server harus menyediakan layanan remote procedure. Pendekatan yang dilakuan adalah sebuah server membuka socket,
lalu menunggu client yang meminta prosedur yang disediakan oleh server. Berikut fitur yang disediakan dari `Kalkulator` sederhana yang dibangun:
<ul>
<li> Operasi Penjumlahan </li>
<li> Operasi Pengurangan </li>
<li> Operasi Perkalian </li>
<li> Operasi Pembagian </li>
<li> Operasi Perpangkatan </li>
<li> Operasi Akar kuadrat </li>
<li> Operasi Trigonometri Sin </li>
<li> Operasi Trigonometri Cos </li>
<li> Dapat dikases pada komputer lain </li>
<li> Dapat melakukan operasi dengan lebih dari dua angka, khusus untuk operasi penjumlahan, pengurangan, perkalian, dan pembagian </li>
<li> Tampilan sederhana sehingga mudah digunakan </li>
</ul>

YANG PERLU DIPERSIAPKAN
----------------
<ul>
<li> Clone atau Donwload file .ZIP </li>
<li> Menginstall Python3 dan pip3. Sistem ini dapat dijalankan baik di Windows maupun Linux. </li>
<li> Jika menggunakan satu komputer, dapat menggunakan ip localhost atau 127.0.0.1 </li>
<li> Jika menggunakan dua komputer berbeda, menggunakan IP dengan terhubung pada wifi yang sama </li>
</ul>

MULAI MENGGUNAKAN APLIKASI
----------------
<ul>
<li> run "server.py" pada terminal </li>
<li> run "client.py" pada terminal lainnya </li>
<li> Masukkan pilihan menu yang diinginkan </li>
<li> Masukkan jumlah angka yang ingin dioperasikan </li>
<li> Masukkan pilihan "Y/T" untuk tetap melanjutkan untuk menggunakan aplikasi atau tidak </li>
<li> Pada pemilihan menu, sistem hanya dapat membaca angka sesuai menu yang tersedia</li>
<li> Pada pemilihan jumlah angka yang ingin dioperasikan, sistem hanya dapat membaca inputan angka </li>
<li> Pada pemilihan apakah masih ingin tetap menggunakan program yang ada, sistem hanya dapat membaca inputan "Y" atau "y" untuk ya 
dan "T" atau "t" untuk tidak</li>
</ul>

INSTALL 
--------------
#### Python3
```Python3
sudo apt-get install python3
```
#### Pip3
```pip
sudo apt-get install python3-pip
```

SOURCE CODE
--------------
source code dapat diunduh pada <https://gitlab.com/tubes_kalkulator/kalkulator_rpc>


Developer
-------------
- Khaeruz Zahra // 1301188585
- Putri Cendikia // 1301188582
- Novian Nuraldi // 1301188575
- Nikolas Raditya Kresna // 1301188549